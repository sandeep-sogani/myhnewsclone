from django.shortcuts import render, redirect,get_object_or_404
from django.views.generic import ListView,DetailView,CreateView, View
from .models import news, Vote, Profile,UserProfile
from django.contrib.auth import authenticate, login,get_user_model
from .forms import SignInForm, SignUpForm,UserProfileForm,LinkForm, VoteForm
from django.views.generic.edit import UpdateView, DeleteView,FormView
from django.core.urlresolvers import reverse
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponse, HttpResponseNotFound, Http404,  HttpResponseRedirect
import json


def index(request):
    return render(request, 'news/home.html')

class LinkListView(ListView):
    model = news
    queryset = news.with_votes.all()
    paginate_by = 5
    def get_context_data(self, **kwargs):
        context = super(LinkListView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            voted = Vote.objects.filter(voter=self.request.user)
            links_in_page = [link.id for link in context["object_list"]]
            voted = voted.filter(link_id__in=links_in_page)
            voted = voted.values_list('link_id', flat=True)
            context["voted"] = voted
        return context

class LinkCreateView(CreateView):
    model = news
    form_class = LinkForm
    template_name = 'news/link_form.html'
    success_url = ('/')

    def form_valid(self, form):
        f = form.save(commit=False)
        f.submitter = self.request.user
        form.save()
        return super(LinkCreateView, self).form_valid(form)

class LinkDetailView(DetailView):
    model = news
    template_name = 'news/link_detail.html'


class LinkUpdateView(UpdateView):
    model = news
    form_class = LinkForm
    template_name = 'news/link_form.html'

class LinkDeleteView(DeleteView):
    model = news
    template_name = 'news/link_delete.html'
    success_url = ('/')



class SignUpView(CreateView):
    model = Profile
    form_class = SignUpForm
    template_name = 'registration.html'
    #success_url = 'home'

    def form_valid(self, form):
        form.save()
        #messages.info(request, "Thanks for registering. You are now logged in.")
        user = authenticate(username=form.cleaned_data['username'],
                            password=form.cleaned_data['password1'],
                            )
        login(self.request, user)
        return redirect('home')
        #return super(SignUpView, self).form_valid(form)


class UserProfileDetailView(DetailView):
    model = get_user_model()
    slug_field = "username"
    template_name = "user_detail.html"

    def get_object(self, queryset=None):
        user = super(UserProfileDetailView, self).get_object(queryset)
        UserProfile.objects.get_or_create(user=user)
        return user

class UserProfileEditView(UpdateView):
    model = UserProfile
    form_class = UserProfileForm
    template_name = "edit_profile.html"

    def get_object(self, queryset=None):
        return UserProfile.objects.get_or_create(user=self.request.user)[0]

    def get_success_url(self):
        return reverse("profile", kwargs={"slug": self.request.user})


class Login(FormView, View):

    template_name = 'news/login.html'
    form_class = AuthenticationForm
    success_url = '/'

    def dispatch(self, request, *args, **kwargs):

        if request.user.is_authenticated():
            return HttpResponseRedirect("/")
        else:
            return super(Login, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(Login, self).form_valid(form)

class JSONFormMixin(object):
    def create_response(self, vdict=dict(), valid_form=True):
        response = HttpResponse(json.dumps(vdict), content_type='application/json')
        response.status = 200 if valid_form else 500
        return response

class VoteFormBaseView(FormView):
    form_class = VoteForm
    success_url= ('/')

    def create_response(self, vdict=dict(), valid_form=True):
        response = HttpResponse(json.dumps(vdict))
        response.status = 200 if valid_form else 500
        return response

    def form_valid(self, form):
        link = get_object_or_404(news, pk=form.data["link"])
        user = self.request.user
        prev_votes = Vote.objects.filter(voter=user, link=link)
        has_voted = (len(prev_votes) > 0)

        ret = {"success": 1}
        if not has_voted:
            # add vote
            v = Vote.objects.create(voter=user, link=link)
            ret["voteobj"] = v.id
        else:
            # delete vote
            prev_votes[0].delete()
            ret["unvoted"] = 1
        return self.create_response(ret, True)


    def form_invalid(self, form):
        ret = {"success": 0, "form_errors": form.errors }
        return self.create_response(ret, False)

class VoteFormView(JSONFormMixin, VoteFormBaseView):
    pass
