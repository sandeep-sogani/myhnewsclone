jQuery(document).ready(function($) 
    {
	$(".vote_form").submit(function(e) 
		{
		    e.preventDefault(); 
		    var btn = $("button", this);
		    var l_id = $(".hidden_id", this).val();
		    btn.attr('disabled', true);
		    $.post("/vote/", $(this).serializeArray(),
			  function(data) {
			      if(data["voteobj"]) {
				  btn.text("-");
				  window.location.reload();
			      }
			      else {
				  btn.text("+");
				  window.location.reload();
			      }
			  });

		    btn.attr('disabled', false);
		});
	$(".url_form").submit(function(e) 
		{
		    e.preventDefault(); 
		    
		});
    });