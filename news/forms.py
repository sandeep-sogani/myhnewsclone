from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django import forms
from .models import news, Vote
import requests


class SignUpForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

class SignInForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(SignInForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'


class UserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput)
	class Meta:
		model = User
		fields = ['username','email','password']

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = ("user",)

class LinkForm(forms.ModelForm):
    error_message = {
        'url_exists': 'The url alredy exists'
    }
    class Meta:
        model = news
        exclude = ("submitter", "rank_score")
        fields = ('title','url')
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'url': forms.TextInput(attrs={'class': 'form-control'})
        }

    def clean(self):
        cleaned_data = super(LinkForm, self).clean()
        print(cleaned_data)

    def clean_url(self):
        url = self.cleaned_data['url']
        request = requests.get(url)
        if request.status_code != 200:
            raise forms.ValidationError('The URL is invalid')
        if news.objects.filter(url=url).exists():
            raise forms.ValidationError('The URL alredy exists')
        return url

class VoteForm(forms.ModelForm):
    class Meta:
        model = Vote
        exclude = ("user",)