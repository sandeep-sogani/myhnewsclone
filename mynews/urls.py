"""mynews URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required as auth
from django.contrib import admin
from news.views import SignUpView,Login, UserProfileDetailView,UserProfileEditView
from news.views import LinkCreateView,LinkDetailView, LinkDeleteView,LinkUpdateView
from news.views import VoteFormView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', include('news.urls')),
    url(r'^accounts', include('registration.backends.hmac.urls')),
    url(r"^users/(?P<slug>\w+)/$", UserProfileDetailView.as_view(),
        name="profile"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name="logout"),
    url(r'^register',SignUpView.as_view(), name='register'),
    url(r"edit_profile/$", auth(UserProfileEditView.as_view()),
        name="edit_profile"),
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^link/create/$',auth(LinkCreateView.as_view()), name="link_create"),
    url(r"^link/(?P<pk>\d+)$", LinkDetailView.as_view(),
        name="link_detail"),
    url(r"^link/update/(?P<pk>\d+)/$", auth(LinkUpdateView.as_view()),
        name="link_update"),
    url(r"^link/delete/(?P<pk>\d+)/$", auth(LinkDeleteView.as_view()),
        name="link_delete"),
    url(r'^vote/$', auth(VoteFormView.as_view()), name="vote"),
]
